geopandas==0.11.0
kramersmoyal==0.4
matplotlib==3.5.2
numpy==1.23.0
pandas==1.4.3
scikit-learn==1.1.1
scipy==1.8.1
tqdm==4.64.1
