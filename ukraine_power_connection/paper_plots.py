#!/usr/bin/python3
# -*- coding: utf-8 -*

"""Plot the figures in the paper. Note that the preprocessing 
and pca_analysis scripts have to be run before this one."""

import os
import numpy as np
import pandas as pd

import datetime

import gzip
import pickle

from kramersmoyal import kmc

import matplotlib
from scipy.signal.spectral import welch
matplotlib.rcParams['pgf.texsystem'] = 'pdflatex'
matplotlib.rcParams.update({'font.family': 'serif', 'font.size': 24,
    'axes.labelsize': 24,'axes.titlesize': 28, 'figure.titlesize' : 28,
    'text.latex.preamble': r'\usepackage{amsmath} \usepackage{mathptmx}'})
matplotlib.rcParams['text.usetex'] = True
from matplotlib import pyplot as plt, use
import matplotlib.dates as mdates

import geopandas

from typing import Union


script_path = os.path.dirname(__file__)

# Default Colors
colours = ['#1f78b4','#b2df8a']


if not os.path.exists(script_path + "/plots"):
    os.mkdir(script_path + "/plots")

__authors__ = [r"Leonardo Rydin Gorj\~ao", r"Philipp C. Böttcher"]


def frequency_timeseries_and_distributions(save_fig: bool=False,
                                           location: str = "Bremen", verbose: bool = False):
    """Plot the frequency timeseries, histograms and increment statistic
    recorded in the chosen location.

    Args:
        save_fig (bool, optional): If 'True', save the resulting figure instead of ploting it in matplotlib gui.
        Defaults to False.
        location (str, optional): Can be any of ['Bremen', 'Herzogenrath', 'Lleida', 'Reisach', 'Sibiu']
        if the file has been preprocessed beforehand.
        Defaults to "Bremen".
        The data has been preprocessed and split into different files for different locations.
    """
    

    labels = [r'14\textsuperscript{th} Feb.--16\textsuperscript{th} Mar.',
              r'16\textsuperscript{th} Mar.--15\textsuperscript{th} Apr.']

    df = pd.read_pickle(script_path + "/raw_data/" + 
                        "frequency-100ms_2022-01-01_2022-05-03_{}.pklz".format(location))

    # Cut nan rows
    row_idx_nan = df.isnull()
    len_before = df.shape[0]
    df = df.loc[~row_idx_nan]
    
    if verbose:
        print("Percentage of NaN rows deleted: " + 
              "{0:.4f}%".format((row_idx_nan.sum()/len_before)*100.))

    data = (df - 50)*1000
    time = df.index
    
    # Condition this to a subset of time before and after
    mask_before = np.array((time >= '2022-02-14 00:00:00') &
                        (time <  '2022-03-16 00:00:00'))
    mask_after = np.array((time >= '2022-03-16 00:00:00') &
                        (time <  '2022-04-15 00:00:00'))
    

    # Histograms of the frequency data
    hist_kwargs = dict(bins=150, density=True)
    hist_before, edge_before = np.histogram(data[mask_before], **hist_kwargs);
    edge_before = (edge_before[1:] + edge_before[:-1])/2
    hist_after, edge_after = np.histogram(data[mask_after], **hist_kwargs);
    edge_after = (edge_after[1:] + edge_after[:-1])/2

    hist_before_inc, edge_before_inc = np.histogram(np.gradient(data[mask_before]),
                                                    **hist_kwargs);
    edge_before_inc = (edge_before_inc[1:] + edge_before_inc[:-1])/2
    hist_after_inc, edge_after_inc = np.histogram(np.gradient(data[mask_after]),
                                                  **hist_kwargs);
    edge_after_inc = (edge_after_inc[1:] + edge_after_inc[:-1])/2

    # Plotting
    fig, ax = plt.subplots(2, 2, figsize=(8,8))

    ax[0,0].plot(time[mask_before], data[mask_before],
                color=colours[0], label=labels[0], lw=1)
    ax[0,0].plot(time[mask_after], data[mask_after],
                color=colours[1], label=labels[1], lw=1)

    ax[0,1].plot(edge_before, hist_before,
                color=colours[0], label=labels[0], lw=3)
    ax[0,1].plot(edge_after, hist_after,
                color=colours[1], label=labels[1], lw=3)

    ax[1,0].plot(edge_before, hist_before,
                color=colours[0], label=labels[0], lw=3)
    ax[1,0].plot(edge_after, hist_after,
                color=colours[1], label=labels[1], lw=3)

    ax[1,1].plot(edge_before_inc, hist_before_inc,
                color=colours[0], label=labels[0], lw=3)
    ax[1,1].plot(edge_after_inc, hist_after_inc,
                 color=colours[1], label=labels[1], lw=3)

    ax[1,0].set_yscale('log')
    ax[1,1].set_yscale('log')

    myFmt = mdates.DateFormatter('%d %b')
    ax[0, 0].xaxis.set_major_formatter(myFmt)
    ax[0, 0].set_xticks([datetime.datetime(2022, 2, 14),  
                         datetime.datetime(2022, 3, 16),
                         datetime.datetime(2022, 4, 15)])
    
    ax[0,0].set_ylabel(r'$f(t)-50\,$Hz / mHz')

    ax[0,1].set_xlabel(r'$f(t)-50\,$Hz / mHz')
    ax[0,1].set_ylabel(r'$\rho(f)$')

    ax[1,0].set_xlabel(r'$f(t)-50\,$Hz / mHz')
    ax[1,0].set_ylabel(r'$\rho(f)$')

    ax[1,1].set_xlabel(r'$\Delta f_{\tau=1\,\mathrm{s}}$ / mHz')
    ax[1,1].set_ylabel(r'$\rho(\Delta f_{\tau=1\,\mathrm{s}})$')

    ax[0,0].set_ylim([None, None])
    ax[0,1].set_ylim([None, 0.021])
    ax[1,0].set_ylim([2e-6, .03])
    ax[1,1].set_ylim([2e-6, 8])
    ax[1,1].set_xlim([-4, 4])

    y_major = matplotlib.ticker.LogLocator(base = 10.0, numticks = 10)
    ax[1,0].yaxis.set_major_locator(y_major)
    ax[1,1].yaxis.set_major_locator(y_major)
    y_minor = matplotlib.ticker.LogLocator(base=10.0, subs=np.arange(1, 10)*.1,
                                           numticks=10)
    ax[1,0].yaxis.set_minor_locator(y_minor)
    ax[1,1].yaxis.set_minor_locator(y_minor)

    ax[0,0].legend(loc='upper center', fontsize=20, ncol=2, handlelength=.5,
                handletextpad=.1, columnspacing=.5, bbox_to_anchor=(1.2,1.3))

    fig.subplots_adjust(left=.155, bottom=.1, right=.975, top=.9,
                        hspace=.5, wspace=.5)
    
    if save_fig:
        fig.savefig(script_path + "/plots/Fig_1.pdf", transparent=True)
        fig.clear()
        
        plt.close(fig)
        
    else:
        plt.show()
    
    return


def frequency_drift_and_diffusion(save_fig: bool = False,
                                  location: str = "Bremen", verbose: bool = False):
    """Print the drift and diffusion for the given location.

    Args:
        save_fig (bool, optional): If 'True', save the plot to a file instead of
        showing it in GUI. Defaults to False.
        location (str, optional): Name of location of the measurement station that has to
        be present in the data set. Defaults to "Bremen".
        verbose (bool, optional): If 'True', plot additional info. Defaults to False.
    """
    
    labels = [r'14\textsuperscript{th} Feb.--16\textsuperscript{th} Mar.',
              r'16\textsuperscript{th} Mar.--15\textsuperscript{th} Apr.']

    df = pd.read_pickle(script_path + 
                        "/raw_data/frequency-100ms_2022-01-01_2022-05-03_{}.pklz".format(location))

    # Cut nan rows
    row_idx_nan = df.isnull()
    len_before = df.shape[0]
    df = df.loc[~row_idx_nan]
    
    if verbose:
        print("Percentage of NaN rows deleted: " + 
              "{0:.4f}%".format((row_idx_nan.sum()/len_before)*100.))
    
    data = (df - 50)*1000
    time = df.index

    # Condition this to a subset of time before and after
    mask_before = np.array((time >= '2022-02-14 00:00:00') &
                        (time <  '2022-03-16 00:00:00'))
    mask_after = np.array((time >= '2022-03-16 00:00:00') &
                        (time <  '2022-04-15 00:00:00'))


    # Kramers-Moyal coefficients (drift and diffusion)
    bins = np.array([5000])

    kmcs_before, edges_before = kmc.km(data[mask_before], bw=2.5, bins=bins,
                                    powers=[0, 1, 2])
    kmcs_after, edges_after = kmc.km(data[mask_after], bw=2.5, bins=bins,
                                    powers=[0, 1, 2])
    
    # Bootstrapping
    kmcs_before_ = np.zeros((3,5000,4))
    kmcs_after_ = np.zeros((3,5000,4))
    edges_before_ = np.zeros((5000,4))
    edges_after_ = np.zeros((5000,4))
    
    div = int(data[mask_after].size / 4)
    for ii in range(4):
        kmcs_before_[..., ii], edge_bef_r = kmc.km(data[mask_before][ii * div:(ii+1) * div],
                                        bw=5.5, bins=bins, powers=[0, 1, 2])
        edges_before_[..., ii] = edge_bef_r[0]
        kmcs_after_[..., ii], edge_aft_r = kmc.km(data[mask_after][ii * div:(ii+1) * div],
                                    bw=5.5, bins=bins, powers=[0, 1, 2])
        edges_after_[..., ii] = edge_aft_r[0]
    
    kmcs_before_std = np.std(kmcs_before_, axis=2)
    kmcs_after_std = np.std(kmcs_after_, axis=2)

    kmcs_before_std_2 = np.std(kmcs_before_[2] - 0.5 * kmcs_before_[1]**2, axis=1)
    kmcs_after_std_2 = np.std(kmcs_after_[2] - 0.5 * kmcs_after_[1]**2, axis=1)

    # Plot it
    fig, ax = plt.subplots(1, 2, figsize=(16,5))

    e_mask_before = (edges_before[0] > -80) & (edges_before[0] < 80)
    e_mask_after = (edges_after[0] > -80) & (edges_after[0] < 80)

    ax[0].plot(edges_before[0][e_mask_before], kmcs_before[1,e_mask_before],
            label=labels[0], color=colours[0], lw=3)
    ax[0].fill_between(edges_before[0][e_mask_before],
                       kmcs_before[1,e_mask_before] - kmcs_before_std[1,e_mask_before],
                       kmcs_before[1,e_mask_before] + kmcs_before_std[1,e_mask_before],
                       color=colours[0], lw=0, alpha=0.1)
    ax[0].plot(edges_before[0][e_mask_before],
               kmcs_before[1,e_mask_before] - kmcs_before_std[1,e_mask_before], ':',
               color=colours[0], lw=2)
    ax[0].plot(edges_before[0][e_mask_before],
               kmcs_before[1,e_mask_before] + kmcs_before_std[1,e_mask_before], ':',
               color=colours[0], lw=2)

    ax[0].plot(edges_after[0][e_mask_after], kmcs_after[1,e_mask_after],
               label=labels[1], color=colours[1], lw=3)
    ax[0].fill_between(edges_after[0][e_mask_after],
                       kmcs_after[1,e_mask_after] - kmcs_after_std[1,e_mask_after],
                       kmcs_after[1,e_mask_after] + kmcs_after_std[1,e_mask_after],
                       color=colours[1], lw=0, alpha=0.1)
    ax[0].plot(edges_after[0][e_mask_after],
               kmcs_after[1,e_mask_after] - kmcs_after_std[1,e_mask_after], ':',
               color=colours[1], lw=2)
    ax[0].plot(edges_after[0][e_mask_after],
               kmcs_after[1,e_mask_after] + kmcs_after_std[1,e_mask_after], ':',
               color=colours[1], lw=2)

    diffusion_before = kmcs_before[2,e_mask_before] - (kmcs_before[1,e_mask_before]**2)/2
    ax[1].plot(edges_before[0][e_mask_before], diffusion_before,
               label=labels[0], color=colours[0], lw=3)
    ax[1].fill_between(edges_before[0][e_mask_before],
                       diffusion_before - kmcs_before_std_2[e_mask_before],
                       diffusion_before + kmcs_before_std_2[e_mask_before],
    color=colours[0], lw=0, alpha=0.1)
    ax[1].plot(edges_before[0][e_mask_before],
               diffusion_before - kmcs_before_std_2[e_mask_before], ':',
               color=colours[0], lw=2)
    ax[1].plot(edges_before[0][e_mask_before],
               diffusion_before + kmcs_before_std_2[e_mask_before], ':',
               color=colours[0], lw=2)

    diffusion_after = kmcs_after[2,e_mask_after] - (kmcs_after[1,e_mask_after]**2)/2
    ax[1].plot(edges_after[0][e_mask_after], diffusion_after,
               label=labels[1], color=colours[1], lw=3)
    ax[1].fill_between(edges_after[0][e_mask_after],
                       diffusion_after - kmcs_after_std_2[e_mask_after],
                       diffusion_after + kmcs_after_std_2[e_mask_after],
    color=colours[1], lw=0, alpha=0.1)
    ax[1].plot(edges_after[0][e_mask_after],
               diffusion_after - kmcs_after_std_2[e_mask_after], ':',
               color=colours[1], lw=2)
    ax[1].plot(edges_after[0][e_mask_after],
               diffusion_after + kmcs_after_std_2[e_mask_after], ':',
               color=colours[1], lw=2)

    ax[0].fill_betweenx([-0.0095, 0.0095],
                        -10, 10, fc='black', interpolate=True, alpha=.1)
    ax[1].fill_betweenx([0, 1], -10, 10, fc='black', interpolate=True, alpha=.1)

    ax[0].set_ylim([np.min(kmcs_before[1,e_mask_before]) * 1.1,
                    np.max(kmcs_before[1,e_mask_before]) * 1.7])
    ax[0].set_ylim(-0.0095, 0.0095)
    
    if location == "Bremen":
        ax[1].set_ylim(.044, .066)
    else:
        diffusion_before 
        ax[1].set_ylim(min(np.min(diffusion_before), np.min(diffusion_after))*0.95,
                       max(np.max(diffusion_before), np.max(diffusion_after))*1.05)

    ax[0].set_yticks([-0.008,-0.004,0.0,0.004,0.008])

    ax[0].set_ylabel(r'primary control $D_1(f)$')
    ax[0].set_xlabel(r'$f(t)-50\,$Hz / mHz')

    ax[1].set_ylabel(r'diffusion $D_2(f)$')
    ax[1].set_xlabel(r'$f(t)-50\,$Hz / mHz')

    ax[0].legend(loc='upper center', ncol=2, handlelength=.5,
                 handletextpad=.4, fontsize=22)
    ax[1].legend(loc='upper center', ncol=2, handlelength=.5,
                 handletextpad=.4, fontsize=22)
    
    if location in ["Bremen", "Herzogenrath"]:
        fig.text(.105, .2, r'{}, Germany'.format(location))
    else:
        fig.text(.15, .2, r'{}'.format(location))
    
    fig.subplots_adjust(left=.1, bottom=.16, right=.995, top=.99,
                        hspace=.1, wspace=.3)
    if save_fig:
        if location == "Bremen":
            fig.savefig(script_path + "/plots/Fig_2.pdf", transparent=True)
        else:
            fig.savefig(script_path + "/plots/drift_n_diffusion_{}.pdf".format(location),
                        transparent=True)
        fig.clear()
        
        plt.close(fig)
        
    else:
        
        plt.show()
        
    return


def ax_pca_frequency_on_map(ax: matplotlib.axes, frequency_components: np.ndarray, explained_ratio: np.ndarray,
                            comp_nn: int, loc_names: list, 
                            xlims: tuple = (-15, 42), ylims: tuple = (30, 75),
                            label_fontsize: int = 18, label_on_top: bool = True):
    """Plot the frequency component given by PCA to the map of Europe.

    Args:
        ax: Matplotlib axis to plot to.
        frequency_components (numpy.ndarray): Matrix that collects all the frequency components.
        explained_ratio (_type_): Array that has the explained variances for all frequency components.
        comp_nn (_type_): Choice of component that will be plotted.
        loc_names (_type_): Name of locaitons in order that is also used in the given arrays.
        xlims (tuple, optional): Longitude range. Defaults to (-15, 42).
        ylims (tuple, optional): Lattitude range. Defaults to (30, 75).
        label_fontsize (int, optional): _description_. Defaults to 18.
        label_on_top (bool, optional): _description_. Defaults to True.
    """
    
    loc_dict = {"Bremen": (53.166635, 8.650304 ),
                "Herzogenrath": (50.863403, 6.091638),
                "Lleida": (41.617699, 0.625771),
                "Reisach": (46.666291, 13.163563),
                "Sibiu": (45.790653, 24.133143)}
    
    world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))
    
    world.boundary.plot(ax=ax, color="gray", lw=1.)
    
    component_r = frequency_components[comp_nn]
    print(component_r)
    
    pos_list = np.array([loc_dict[xx] for xx in loc_names])
    
    ax.scatter(pos_list[:, 1], pos_list[:, 0], c=component_r,
               vmin=-1, vmax=1, cmap='PiYG', edgecolor="k",
               alpha=1., s=50, zorder=5)
    
    # Aesthetics
    ax.set_xlim(left=min(xlims), right=max(xlims))
    ax.set_ylim(bottom=min(ylims), top=max(ylims))
    ax.set_aspect('auto')
    
    if label_on_top:
        ax.set_title("$\\tilde{\\lambda}_" + "{" + "{}".format(comp_nn+1) + "}^f" + "={0:.4f}".format(explained_ratio[comp_nn]) + "$",
                     fontsize=label_fontsize)
        
    else:
        ax.set_title("$\\tilde{\\lambda}_" + "{" + "{}".format(comp_nn+1) + "}^f" + "={0:.4f}".format(explained_ratio[comp_nn]) + "$",
                     fontsize=label_fontsize, y=-0.175)
        
    return
    
    
def compare_years_PCs(save_fig: bool = False, nr_comps: int = 2):
    """Plot principal components of measured frequency for the year 2022 and a 
    reference year on the map of Continental Europe. 

    Args:
        save_fig (bool, optional): If 'True', save the figure instead of
        showing it in GUI. Defaults to False.
        nr_comps (int, optional): Number of principal components that will be shown.
        Defaults to 2.
    """
    
    # Load data
    file_path_ref = script_path + "/data/freq_pca_reflong.pklz"
    file_path = script_path + "/data/freq_pca_long.pklz"
    
    with gzip.open(file_path, "rb") as fh_norm, gzip.open(file_path_ref) as fh_ref:
        [tt_ax, loc_names, _, 
         all_comp, variance_ratio, amplitudes] = pickle.load(fh_norm)
        
        [tt_ax_ref, loc_names_ref, _, 
         all_comp_ref, variance_ratio_ref, amplitudes_ref] = pickle.load(fh_ref)
    
    # Plots
    
    # PCs 
    fig, ax = plt.subplots(2, nr_comps, figsize=(5*nr_comps, 6))
    for idx_r in range(nr_comps):
        
        # Compare direction of all_comp
        for idx_c in range(len(all_comp)):
            norm_vec_r = all_comp[idx_c]
            ref_vec_r = all_comp_ref[idx_c]
            
            angle_now = np.arccos(np.dot(norm_vec_r, ref_vec_r))
            angle_other_sign = np.arccos(np.dot(norm_vec_r, -1*ref_vec_r))
            
            if angle_other_sign <  angle_now:
                all_comp_ref[idx_c] *= -1
        
        ax_pca_frequency_on_map(ax[1, idx_r], all_comp, variance_ratio,
                                idx_r, loc_names, label_on_top=False)
        ax_pca_frequency_on_map(ax[0, idx_r], all_comp_ref, variance_ratio_ref,
                                idx_r, loc_names_ref, label_on_top=False)

        #ax[0, idx_r].axis("off")
        
        ax[0, idx_r].tick_params(left=False,
                bottom=False,
                labelleft=False,
                labelbottom=False)
        ax[1, idx_r].tick_params(left=False,
                bottom=False,
                labelleft=False,
                labelbottom=False)
    
    #fig.subplots_adjust(right=.9)
    # Mutual colormap
    pos_top = ax[0, -1].get_position()
    pos_bottom = ax[1, -1].get_position()
    axi_0 = fig.add_axes([0.925, pos_bottom.y0, 0.02, pos_top.y1 - pos_bottom.y0])
    
    sm = plt.cm.ScalarMappable(cmap=plt.cm.PiYG, norm=plt.Normalize(-1, 1))
    sm._A = []
    cbar = fig.colorbar(sm, cax=axi_0)
    #cbar.set_label("$\\Re{(\\mu_2)}$", rotation=0, labelpad=20)
    fig.subplots_adjust(right=.91)
    cbar.set_ticks([-1, 0, 1])
    #cbar.set_ticklabels(["small", "large"])
    
    axi_0.text(-1.5, 1.05, "$v_{n}(x)$", fontsize=30,
              weight='bold', transform=axi_0.transAxes)
    
    ax[0, 0].set_ylabel("2021")
    ax[1, 0].set_ylabel("2022")
    
    # Label panels
    panel_label_ls = [r"\textbf{a}", r"\textbf{b}", r"\textbf{c}", r"\textbf{d}",
                      r"\textbf{e}", r"\textbf{f}", r"\textbf{g}", r"\textbf{h}",
                      r"\textbf{i}", r"\textbf{j}", r"\textbf{k}", r"\textbf{l}"]
    shift = .15
    for idx_r, ax_r in enumerate(ax.flatten()):
        ax_r.text(0 - shift, 1.1, panel_label_ls[idx_r],
                  fontsize=30, weight='bold', verticalalignment='center',
                  transform=ax_r.transAxes)
    
    if save_fig:
        fig_path = script_path + "/plots/Fig_3_1.pdf".format(nr_comps)
        fig.savefig(fig_path)
        
        fig.clear()
        plt.close(fig)
    
    else:
        plt.show()
    
    return


def pca_inter_area_modes_PSD(comp_nn: int = 1, nperseg_fac: int = 8,
                             freq_lims: tuple = (5*10**-2, 1), 
                             linewidth: int = 3, save_fig: bool = False):
    """Compare 2021 and 2022 by showing PSD of amplitudes of PCA analysis.

    Args:
        comp_nn (int, optional): Which PC to use for analysis. Defaults to 1 (i.e. the inter-area mode).
        nperseg_fac (int, optional): parameter of welch function 
        see scipy.signal.spectral.welch for documentation. Defaults to 8.
        freq_lims (tuple, optional): Limits for x-axis in plot. Defaults to (5*10**-2, 1).
        linewidth (int, optional): Linewidth of plot. Defaults to 3.
        save_fig (bool, optional): If True, save plot instead of showing it to GUI. Defaults to False.
    """
    
    color_ls = ['#1f78b4','#b2df8a']
    
    def max_min_idx(ff_ax, ff_min, ff_max):
        
        idx_min = np.argmin(abs(ff_ax - ff_min))
        idx_max = np.argmin(abs(ff_ax - ff_max))
        
        return idx_min, idx_max
    
    tt_con_ref = datetime.datetime(2021, 3, 16)
    tt_con_norm = datetime.datetime(2022, 3, 16)
    
    # Load data
    file_path_ref = script_path + "/data/freq_pca_reflong.pklz"
    file_path = script_path + "/data/freq_pca_long.pklz"
    
    with gzip.open(file_path, "rb") as fh_norm, gzip.open(file_path_ref) as fh_ref:
        [tt_ax, loc_names, _, 
         _, _, amplitudes] = pickle.load(fh_norm)
        
        [tt_ax_ref, loc_names_ref, _, 
         _, _, amplitudes_ref] = pickle.load(fh_ref)
        
    # Normal side
    tt_ax_split_idx = np.argmin(abs(tt_ax - tt_con_norm).total_seconds())
    
    amp_bef = amplitudes[:tt_ax_split_idx]
    amp_aft = amplitudes[tt_ax_split_idx:]
    
    ff_bef, PSD_bef = welch(amp_bef[:, comp_nn], fs=10, nperseg=int(1024*nperseg_fac))
    ff_aft, PSD_aft = welch(amp_aft[:, comp_nn], fs=10, nperseg=int(1024*nperseg_fac))
    
    # Ref side
    tt_ax_split_idx_ref = np.argmin(abs(tt_ax_ref - tt_con_ref).total_seconds())
    
    amp_bef_ref = amplitudes_ref[:tt_ax_split_idx_ref]
    amp_aft_ref = amplitudes_ref[tt_ax_split_idx_ref:]
        
    ff_bef_ref, PSD_bef_ref = welch(amp_bef_ref[:, comp_nn], fs=10, nperseg=1024*4)
    ff_aft_ref, PSD_aft_ref = welch(amp_aft_ref[:, comp_nn], fs=10, nperseg=1024*4)
        
    
    # Plot
    fig, ax = plt.subplots(2, 1, figsize=(5, 6), sharex='all', sharey='all')
    
    ax[0].loglog(ff_bef_ref, PSD_bef_ref, label="Before 16\\textsuperscript{th} March", color=color_ls[0], lw=linewidth)
    ax[0].loglog(ff_aft_ref, PSD_aft_ref, label="After 16\\textsuperscript{th} March", color=color_ls[1], lw=linewidth)
    
    ax[1].loglog(ff_bef, PSD_bef, label="Before", color=color_ls[0], lw=linewidth)
    ax[1].loglog(ff_aft, PSD_aft, label="After", color=color_ls[1], lw=linewidth)
    
    # Mark maximum in both plots
    ff_range = (0.03, .35)
    idx_lim_bef = max_min_idx(ff_bef, ff_range[0], ff_range[1])
    idx_lim_aft = max_min_idx(ff_aft, ff_range[0], ff_range[1])
    
    idx_max_bef = np.argmax(PSD_bef[idx_lim_bef[0]:idx_lim_bef[1]])
    idx_max_aft = np.argmax(PSD_aft[idx_lim_aft[0]:idx_lim_aft[1]])
    
    idx_lim_bef_ref = max_min_idx(ff_bef_ref, ff_range[0], ff_range[1])
    idx_lim_aft_ref = max_min_idx(ff_aft_ref, ff_range[0], ff_range[1])
    
    idx_max_bef_ref = np.argmax(PSD_bef_ref[idx_lim_bef_ref[0]:idx_lim_bef_ref[1]])
    idx_max_aft_ref = np.argmax(PSD_aft_ref[idx_lim_aft_ref[0]:idx_lim_aft_ref[1]])

    freq_bef_max = ff_bef[idx_lim_bef[0]+ idx_max_bef]
    freq_aft_max = ff_aft[idx_lim_aft[0] + idx_max_aft]
    
    freq_bef_max_ref = ff_bef_ref[idx_lim_bef_ref[0]+ idx_max_bef_ref]
    freq_aft_max_ref = ff_aft_ref[idx_lim_aft_ref[0] + idx_max_aft_ref]
    
    ax[1].axvline(x=freq_bef_max, color=color_ls[0], linestyle="--", lw=linewidth)
    ax[1].axvline(x=freq_aft_max, color=color_ls[1], linestyle=":", lw=linewidth)
    
    ax[0].axvline(x=freq_bef_max_ref, color=color_ls[0], linestyle="--", lw=linewidth)
    ax[0].axvline(x=freq_aft_max_ref, color=color_ls[1], linestyle=":", lw=linewidth)

    # Print maxima 
    print("-------")
    print("2021")
    print(1/freq_bef_max_ref)
    print(1/freq_aft_max_ref)
    
    print("\n\n2022")
    print(1/freq_bef_max)
    print(1/freq_aft_max)
    print("-------")

    # Aesthetics
    for ax_r in ax:
        ax_r.set_xlim(left=min(freq_lims), right=max(freq_lims))
        ax_r.set_ylabel("$\\mathcal{S}[A_" + "{}]$".format(comp_nn+1))
        
    ax[1].set_xlabel("$\\zeta$ / $s^{-1}$", labelpad=-10)
    ax[0].legend(numpoints=None, fontsize=12, handlelength=.5, handletextpad=.2)
    
    # Label panels
    panel_label_ls = [r"\textbf{e}", r"\textbf{f}", r"\textbf{g}"]
    shift = .15
    for idx_r, ax_r in enumerate(ax.flatten()):
        ax_r.text(0 - shift, 1.1, panel_label_ls[idx_r],
                  fontsize=30, weight='bold', verticalalignment='center',
                  transform=ax_r.transAxes)
        
    fig.subplots_adjust(left=0.25)
    
    if save_fig:
        fig_path = script_path + "/plots/Fig_3_2.pdf"
        
        fig.savefig(fig_path, bbox_inches='tight', transparent=True)
        fig.clear()
        plt.close(fig)
        
    else:
        plt.show()
    
    
    return


def power_exchange_UA_RS_BY(save_fig: bool = False):
    """Plot the power flows between Ukraine and {Russia, Belarus}.

    Args:
        save_fig (bool, optional): If 'True', save plot instead of showing it to GUI. Defaults to False.
    """
    
    labels = [r'2\textsuperscript{nd}--16\textsuperscript{th} March',
              r'16\textsuperscript{th}--30\textsuperscript{th} March',
              r'2\textsuperscript{nd}--30\textsuperscript{th} March']

    # Direct processcing of the Flow Data from the ENTSO-E transparency platfrom.
    # The data is available in their SFTP server. Search for the 12.1.G code to
    # locate the .csv files for each desired month.

    usecols = ['DateTime', 'ResolutionCode', 'OutAreaTypeCode',
               'OutMapCode', 'InMapCode', 'FlowValue']

    file_ls = ['2022_02_PhysicalFlows_12.1.G.csv', '2022_03_PhysicalFlows_12.1.G.csv', '2022_04_PhysicalFlows_12.1.G.csv']
    data_ls = [pd.read_csv(script_path + '/raw_data/PhysicalFlows_12.1.G/' + xx, sep='\t',
                           usecols=usecols, parse_dates=True) for xx in file_ls]
    
    data = pd.concat(data_ls)

    # Limit the data to CTY and 60 mins resolution and drop does columns
    data = data[data['OutAreaTypeCode'] == 'CTY']
    data = data[data['ResolutionCode'] == 'PT60M']

    # Merge flow directions
    data['from-to'] = data['OutMapCode'] + ' to ' + data['InMapCode']

    # transpose the data (this will drop all unused columns)
    data = data.pivot_table(values='FlowValue', index=data['DateTime'],
                            columns='from-to', aggfunc='first')

    # Limit data to country to country exchange: all countries are coded with their
    # ISO4 2-letter codes.
    data = data[data.columns[np.array([len(e) for e in data.keys().values]) == 8]]

    # subract incoming to outgoing flows
    list_of_keys = []
    for e in data.keys().values:
        if e not in list_of_keys:
            data[e[:2]+'-'+e[-2:]] = data[e] - data[e[-2:] + ' to ' + e[:2]]
            list_of_keys.append(e[-2:] + ' to ' + e[:2])

    data = data[data.columns[np.array([len(e) for e in data.keys().values]) == 5]]

    # Remove few countries with zero exchange
    data = data[data.columns[~(data == 0).all()]]

    # Sort time
    data = data.set_index(pd.to_datetime(data.index))
    data = data.sort_index()

    # Limit temporal range
    data = data[(data.index >= '2022-02-04 00:00:00.000') &
                (data.index < '2022-04-05 00:00:00.000')]

    fig, ax = plt.subplots(1, 1, figsize=(8,6))

    ax.axvline(x=datetime.datetime(2022, 2, 24), linestyle='--', color='r', lw=2 )
    ax.axvline(x=datetime.datetime(2022, 3, 16), linestyle='--', color='k', lw=2 )

    ax.plot(data.index,data['RU-UA'], label='Russia', lw=3, color=colours[0])
    ax.plot(data.index,data['BY-UA'], label='Belarus', lw=3, color=colours[1])
    ax.fill_betweenx([-350,1090], datetime.datetime(2022, 2, 25, 15), 
                     datetime.datetime(2022, 4, 8),
                    color='black', alpha=0.2, lw=0)

    ax.set_ylabel(r'$P_{\mathrm{Russia~\&~Belarus~to~Ukraine}}$ / MW')
    ax.set_xlabel(r'Dates')

    ax.set_ylim([-350, 1090])
    ax.set_xlim([None, datetime.datetime(2022, 4, 8)])

    fig.text(0.38, 0.94, r'Invasion')
    fig.text(0.57, 0.94, r'Synchronization')

    fig.text(0.52, 0.3, r'\textbf{NO DATA}', fontsize=50, rotation=45, c='white')

    ax.set_xticks(['2022-02-04', '2022-02-24', '2022-03-16', '2022-04-05'])
    ax.set_xticklabels([r'4\textsuperscript{th} Feb.',
                        r'24\textsuperscript{th} Feb.',
                        r'16\textsuperscript{th} Mar.',
                        r'5\textsuperscript{th} Apr.'])

    ax.legend(handlelength=.5, handletextpad=.2, ncol=1, columnspacing=.3, loc=4)
    fig.subplots_adjust(left=.15, bottom=.14, right=.97, top=.92,
                        hspace=.1, wspace=.2)
    
    if save_fig:
        fig.savefig(script_path + "/plots/Fig_4.pdf", transparent=True)
        fig.clear()
        plt.close(fig)
        
    else:
        plt.show()
    
    return

def power_exchange_UA_CEU(save_fig: bool = False):
    """Plot power flow between Ukraine and countries in Continental Europe

    Args:
        save_fig (bool, optional): If True, save figure instead of showing it in GUI. Defaults to False.
    """
    
    colour_ls = ['#7570b3', '#e78ac3', '#a6d854', '#66c2a5',
                 '#fc8d62', '#8da0cb']

    # Direct processcing of the Flow Data from the ENTSO-E transparency platfrom.
    # The data is available in their SFTP server. Search for the 12.1.G code to
    # locate the .csv files for each desired month.

    usecols = ['DateTime', 'ResolutionCode', 'OutAreaTypeCode',
            'OutMapCode', 'InMapCode', 'FlowValue']
    
    file_ls = ['2022_02_PhysicalFlows_12.1.G.csv', '2022_03_PhysicalFlows_12.1.G.csv', '2022_04_PhysicalFlows_12.1.G.csv']
    data_ls = [pd.read_csv(script_path + '/raw_data/PhysicalFlows_12.1.G/' + xx, sep='\t',
                           usecols=usecols, parse_dates=True) for xx in file_ls]
    
    data = pd.concat(data_ls)

    # Limit the data to CTY and 60 mins resolution and drop does columns
    data = data[data['OutAreaTypeCode'] == 'CTY']
    data = data[data['ResolutionCode'] == 'PT60M']

    # Merge flow directions
    data['from-to'] = data['OutMapCode'] + ' to ' + data['InMapCode']

    # transpose the data (this will drop all unused columns)
    data = data.pivot_table(values='FlowValue', index=data['DateTime'],
                            columns='from-to', aggfunc='first')

    # Limit data to country to country exchange: all countries are coded with their
    # ISO4 2-letter codes.
    data = data[data.columns[np.array([len(e) for e in data.keys().values]) == 8]]

    # subract incoming to outgoing flows
    list_of_keys = []
    for e in data.keys().values:
        if e not in list_of_keys:
            data[e[:2]+'-'+e[-2:]] = data[e] - data[e[-2:] + ' to ' + e[:2]]
            list_of_keys.append(e[-2:] + ' to ' + e[:2])

    data = data[data.columns[np.array([len(e) for e in data.keys().values]) == 5]]

    # Remove few countries with zero exchange
    data = data[data.columns[~(data == 0).all()]]

    # Sort time
    data = data.set_index(pd.to_datetime(data.index))
    data = data.sort_index()

    # Limit temporal range
    data = data[(data.index >= '2022-02-04 00:00:00.000') &
                (data.index < '2022-04-05 00:00:00.000')]

    # Figure 5
    fig, ax = plt.subplots(1,1,figsize=(8, 6))

    ax_ = ax.inset_axes([0.45, 0.52,
                         0.45, .45])

    fig.text(0.34, 0.94, r'Invasion')
    fig.text(0.50, 0.94, r'Synchronization')

    ax.axvline(x=datetime.datetime(2022, 2, 24), linestyle='--', color='r', lw=2)
    ax.axvline(x=datetime.datetime(2022, 3, 16), linestyle='--', color='k', lw=2)
    ax_.axvline(x=datetime.datetime(2022, 2, 24), linestyle='--', color='r', lw=2)
    ax_.axvline(x=datetime.datetime(2022, 3, 16), linestyle='--', color='k', lw=2)

    ax.plot(data.index, -data['HU-UA'] - data['MD-UA'] - data['PL-UA'] -
                        data['RO-UA'] - data['SK-UA'], color=colour_ls[0])

    ax_.plot([], lw=0, color='w', label=r"$P_\text{Ukraine to}$ :")
    ax_.plot(data.index,-data['RO-UA'], label='Romania', lw=2, color=colour_ls[1])
    ax_.plot(data.index,-data['SK-UA'], label='Slovakia', lw=2, color=colour_ls[2])
    ax_.plot(data.index,-data['HU-UA'], label='Hungary', lw=2, color=colour_ls[3])
    ax_.plot(data.index,-data['MD-UA'], label='Moldova', lw=2, color=colour_ls[4])
    ax_.plot(data.index,-data['PL-UA'], label='Poland', lw=2, color=colour_ls[5])
    
    ax_.set_xticks(['2022-02-04','2022-02-24','2022-03-16','2022-04-05'])
    ax_.set_xticklabels([])

    ax_.set_yticks([-500, 0, 500]);
    ax_.set_yticklabels([-500, 0, 500], fontsize=14, rotation=90)
    ax_.set_xticklabels([r'4\textsuperscript{th} Feb.',
                        r'24\textsuperscript{th} Feb.',
                        r'16\textsuperscript{th} Mar.',
                        r'5\textsuperscript{th} Apr.'],
                        fontsize=14)

    ax_.set_ylabel(r'$P_{\mathrm{Ukraine~to~X}}$ / MW', size=14)
    
    ax.set_ylabel(r'$P_{\mathrm{Ukraine~to~EU}}$ / MW')
    ax.set_xlabel(r'Dates')

    ax.set_xticks(['2022-02-04', '2022-02-24', '2022-03-16', '2022-04-05'])
    ax.set_xticklabels([r'4\textsuperscript{th} Feb.',
                        r'24\textsuperscript{th} Feb.',
                        r'16\textsuperscript{th} Mar.',
                        r'5\textsuperscript{th} Apr.'])

    ax_.legend(handlelength=.5, handletextpad=.2, ncol=1, columnspacing=.3, loc=4,
            fontsize=17,bbox_to_anchor=(1.55, 0.01))
    fig.subplots_adjust(left=.15, bottom=.14, right=.9, top=.92,
                        hspace=.1, wspace=.2)
    if save_fig:
        fig.savefig(script_path + "/plots/Fig_5.pdf", transparent=True)
        fig.clear()
        
        plt.close(fig)
        
    else:
        plt.show()
    
    return


def graphical_abstract(save_fig=False):
    
    fig, ax_arr = plt.subplots(1, 2, figsize=(16, 6))
    ax_pwr = ax_arr[0]
    ax_freq = ax_arr[1]
    
    colour_ls = ['#7570b3', '#e78ac3', '#a6d854', '#66c2a5',
                 '#fc8d62', '#8da0cb']

    # Direct processcing of the Flow Data from the ENTSO-E transparency platfrom.
    # The data is available in their SFTP server. Search for the 12.1.G code to
    # locate the .csv files for each desired month.
    
    ## Power flow side
    usecols = ['DateTime', 'ResolutionCode', 'OutAreaTypeCode',
               'OutMapCode', 'InMapCode', 'FlowValue']
    
    file_ls = ['2022_02_PhysicalFlows_12.1.G.csv', '2022_03_PhysicalFlows_12.1.G.csv', '2022_04_PhysicalFlows_12.1.G.csv']
    data_ls = [pd.read_csv(script_path + '/raw_data/PhysicalFlows_12.1.G/' + xx, sep='\t',
                           usecols=usecols, parse_dates=True) for xx in file_ls]
    
    data = pd.concat(data_ls)

    # Limit the data to CTY and 60 mins resolution and drop does columns
    data = data[data['OutAreaTypeCode'] == 'CTY']
    data = data[data['ResolutionCode'] == 'PT60M']

    # Merge flow directions
    data['from-to'] = data['OutMapCode'] + ' to ' + data['InMapCode']

    # transpose the data (this will drop all unused columns)
    data = data.pivot_table(values='FlowValue', index=data['DateTime'],
                            columns='from-to', aggfunc='first')

    # Limit data to country to country exchange: all countries are coded with their
    # ISO4 2-letter codes.
    data = data[data.columns[np.array([len(e) for e in data.keys().values]) == 8]]

    # subract incoming to outgoing flows
    list_of_keys = []
    for e in data.keys().values:
        if e not in list_of_keys:
            data[e[:2]+'-'+e[-2:]] = data[e] - data[e[-2:] + ' to ' + e[:2]]
            list_of_keys.append(e[-2:] + ' to ' + e[:2])

    data = data[data.columns[np.array([len(e) for e in data.keys().values]) == 5]]

    # Remove few countries with zero exchange
    data = data[data.columns[~(data == 0).all()]]

    # Sort time
    data = data.set_index(pd.to_datetime(data.index))
    data = data.sort_index()

    # Limit temporal range
    data = data[(data.index >= '2022-02-04 00:00:00.000') &
                (data.index < '2022-04-05 00:00:00.000')]

    
    #fig.text(0.34, 0.94, r'Invasion')
    #fig.text(0.50, 0.94, r'Synchronization')

    ax_pwr.axvline(x=datetime.datetime(2022, 2, 24), linestyle='--', color='r', lw=3)
    ax_pwr.axvline(x=datetime.datetime(2022, 3, 16), linestyle='--', color='k', lw=3)

    ax_pwr.plot(data.index, -data['HU-UA'] - data['MD-UA'] - data['PL-UA'] -
                        data['RO-UA'] - data['SK-UA'], color=colour_ls[0])
    
    ax_pwr.set_ylabel(r'Powerflow from Ukraine to EU / MW', size=26)
    ax_pwr.set_xticks(['2022-02-04', '2022-02-24', '2022-03-16', '2022-04-05'])
    ax_pwr.set_xticklabels([r'4\textsuperscript{th} Feb.',
                        r'24\textsuperscript{th} Feb.',
                        r'16\textsuperscript{th} Mar.',
                        r'5\textsuperscript{th} Apr.'])
    
    ## Frequency side
    labels = [r'14\textsuperscript{th} Feb.--16\textsuperscript{th} Mar.',
              r'16\textsuperscript{th} Mar.--15\textsuperscript{th} Apr.']
    location = "Bremen"
    df = pd.read_pickle(script_path + "/raw_data/" + 
                        "frequency-100ms_2022-01-01_2022-05-03_{}.pklz".format(location))

    # Cut nan rows
    row_idx_nan = df.isnull()
    len_before = df.shape[0]
    df = df.loc[~row_idx_nan]

    data = (df - 50)*1000
    time = df.index
    
    # Condition this to a subset of time before and after
    mask_before = np.array((time >= '2022-02-14 00:00:00') &
                        (time <  '2022-03-16 00:00:00'))
    mask_after = np.array((time >= '2022-03-16 00:00:00') &
                        (time <  '2022-04-15 00:00:00'))
    ax_freq.plot(time[mask_before], data[mask_before],
                color=colours[0], label=labels[0], lw=1)
    ax_freq.plot(time[mask_after], data[mask_after],
                color=colours[0], label=labels[1], lw=1)
    ax_freq.set_ylabel(r'$f(t)-50\,$Hz / mHz')
    
    ax_freq.set_xticks(['2022-02-14', '2022-03-16', '2022-04-15'])
    ax_freq.set_xticklabels([r'14\textsuperscript{th} Feb.',
                        r'16\textsuperscript{th} Mar.',
                        r'15\textsuperscript{th} Apr.'])

    ax_freq.axvline(x=datetime.datetime(2022, 2, 24), linestyle='--', color='r', lw=3)
    ax_freq.axvline(x=datetime.datetime(2022, 3, 16), linestyle='--', color='k', lw=3)
    
    #ax_freq.set_ylim(bottom=-100, top=100)
    ax_freq.locator_params(axis='y', nbins=3)
    ax_pwr.locator_params(axis='y', nbins=3)
    
    ax_pwr.text(.25, 1.01, r'Invasion', transform=ax_pwr.transAxes)
    ax_pwr.text(.48, 1.01, r'Synchronization', transform=ax_pwr.transAxes)
    
    ax_freq.text(.1, 1.01, r'Invasion', transform=ax_freq.transAxes)
    ax_freq.text(.325, 1.01, r'Synchronization', transform=ax_freq.transAxes)
    
    
    fig.subplots_adjust(wspace=.3, right=.975)
    
    if save_fig:
        fig_path = script_path + "/plots/graphical_abstract.svg"
        
        fig.savefig(fig_path, transparent=True)
        fig.clear()
        
        plt.close(fig)
        
    else:
        plt.show()
    
    return


def plot_everything():
    """Script to plot every figure at the same time and save them to 
    the 'ukraine_power_connection/plots' folder.
    """
    
    # Fig 1:
    frequency_timeseries_and_distributions(save_fig=True)
    
    # Fig 2:
    frequency_drift_and_diffusion(save_fig=True)
    
    # Fig 3:
    compare_years_PCs(save_fig=True)
    pca_inter_area_modes_PSD(save_fig=True)
    
    # Fig 4:
    power_exchange_UA_RS_BY(save_fig=True)
    
    # Fig 5:
    power_exchange_UA_CEU(save_fig=True)
    
    # Graphical Abstract
    graphical_abstract(save_fig=True)
        
    return
