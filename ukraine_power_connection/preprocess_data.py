#!/usr/bin/python3
# -*- coding: utf-8 -*

import os

import pandas as pd

script_path = os.path.dirname(__file__)

__authors__ = [r"Philipp C. Böttcher"]

def frequency_to_pickle(also_split_files: bool = True):
    """Preprocess frequency by saving as compressed pklz and addionally split
    into different files for different measurement station. 
    This is just done to reduce the filesize and memory usage in the subsequent steps."""
    
    fname_ls = ["frequency-100ms_2021-02-12_2021-04-16.csv",
                "frequency-100ms_2022-01-01_2022-05-03.csv"]
    
    for fname_r in fname_ls:
        df_r = pd.read_csv(script_path + "/raw_data/" + fname_r)
        
        tt_ax = pd.to_datetime(df_r["time"], format='%Y-%m-%d %H:%M:%S.%f')
        
        df_r.index = tt_ax
        df_r.drop(columns=["time"], inplace=True)
        
        df_r.to_pickle(script_path + "/raw_data/" + fname_r.split(".csv")[0] + ".pklz")
        
        if also_split_files:
            for col_r in df_r:
                df_r[col_r].to_pickle(script_path + "/raw_data/" + fname_r.split(".csv")[0] + "_{}.pklz".format(col_r))
        
        del df_r
        
    return

