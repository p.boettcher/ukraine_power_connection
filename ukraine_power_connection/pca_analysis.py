#!/usr/bin/python3
# -*- coding: utf-8 -*

import os

import numpy
import pandas as pd

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

import gzip
import pickle

import datetime

from typing import Union

script_path = os.path.dirname(__file__)

if not os.path.exists(script_path + '/data'):
    os.mkdir(script_path + '/data')

__authors__ = [r"Philipp C. Böttcher"]


def prepare_matrix_for_pca(df: pd.DataFrame, t0: datetime.datetime,
                           tf: datetime.datetime, cut_nan_rows: bool=True,
                           verbose: bool=True) -> tuple((pd.DataFrame, pd.Index, numpy.ndarray)):
    """Prepare the matrix by deleting rows that have any NaN in them.

    Args:
        df (pandas.DataFrame): DataFrame with the frequencies at the locations at different times.
        t0 (datetime.datetime): Starting time.
        tf (datetime.datetime): End time.
        cut_nan_rows (bool, optional): If True, delete rows with nans. Defaults to True.
        verbose (bool, optional): Defaults to True.

    Returns:
        df_cut, index_cut, matrix_values: cut dataframe, index of used values, only values in matrix
    """
    
    df_cut = df[(df.index > t0) & (df.index < tf)].copy()
    
    if cut_nan_rows:
        row_idx_nan = df_cut.isnull().any(axis=1)
        len_before = df_cut.shape[0]
        df_cut = df_cut.loc[~row_idx_nan, :]
        
    index_cut = df_cut.index
    matrix_values = df_cut.values
    
    if verbose:
        print("Percentage of NaN rows deleted: " + 
              "{0:.4f}%".format((row_idx_nan.sum()/len_before)*100.))
    
    return df_cut, index_cut, matrix_values


def pca_of_matrix(freq_matrix: numpy.ndarray, n_components: Union[int, None] = None):
    """Do the PCA analysis of the given freq_matrix that collects the grid frequency

    Args:
        freq_matrix (numpy.ndarray): Array that collect the frequency measured at the different
        locations for different times.
        n_components (integer, optional): Number of used principal componetns. Defaults to None.

    Returns:
        pca, principal_components
    """ 
    
    if n_components is None:
        n_components = freq_matrix.shape[1]
        
    pca = PCA(n_components=n_components)
    
    scaler = StandardScaler(with_std=False)
    freq_trans = scaler.fit_transform(freq_matrix)
    
    principal_components = pca.fit_transform(freq_trans)
    
    return pca, principal_components


def full_frequency_pca(file_name: str, t0: datetime.datetime, tf: datetime.datetime,
                       case_str: str,
                       save_it: bool=True):
    """Load the data and do the PCA for the given parameters.

    Args:
        file_name (str): Path to pickled frequency data.
        t0 (datetime.datetime): Starting time.
        tf (datetime.datetime): End time.
        case_str (_type_): _description_
        save_it (bool, optional): _description_. Defaults to True.
    """
        
    df_in = pd.read_pickle(file_name)
    
    df, tt_ax, freq_vals = prepare_matrix_for_pca(df_in, t0, tf)
    
    loc_names = df.columns

    pca_freq, amplitudes = pca_of_matrix(freq_vals)
    
    res_tup = (tt_ax, loc_names, freq_vals,
               pca_freq.components_, pca_freq.explained_variance_ratio_,
               amplitudes)
    
    if save_it:
        file_path = script_path + "/data/freq_pca_{}.pklz".format(case_str)
                                 
        with gzip.open(file_path, "wb") as fh_out:
            pickle.dump(res_tup, fh_out)
    
def meta_all_pca():
    """Run the PCAs for the results presented in the paper."""
    
    fname22 = (script_path + 
               "/raw_data/frequency-100ms_2022-01-01_2022-05-03.pklz")
    
    fname21 = (script_path + 
               "/raw_data/frequency-100ms_2021-02-12_2021-04-16.pklz")    
    
    case_str_long = "long"
    t0_long = datetime.datetime(2022, 2, 12)
    tf_long = datetime.datetime(2022, 4, 16)
    full_frequency_pca(fname22, t0_long, tf_long, case_str_long)
    
    ref_case_str_long = "reflong"
    ref_t0_long = datetime.datetime(2021, 2, 12)
    ref_tf_long = datetime.datetime(2021, 4, 16)
    full_frequency_pca(fname21, ref_t0_long, ref_tf_long, ref_case_str_long)
    
    return
