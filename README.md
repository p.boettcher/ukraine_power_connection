# Ukraine Power Connection

This package includes the python code to calclulate the results and recreate the plots shown in the publication on [ArXiv](https://arxiv.org/abs/2204.07508).

## Installation and Setup

We recommend using anaconda and use the following command to setup the environment

```bash
usr@machine:~$ conda create --name ua_power -c conda-forge python=3.8 pip
usr@machine:~$ conda activate ua_power
(ua_power) usr@machine:~$ pip install -r requirements.txt
```


### Data

The results depend on frequency data recorded by [GridRadar](https://gridradar.net) and data on power flows are available from the  [Entso-E transparency platform](https://transparency.entsoe.eu).

Data for one location (Bremen, Germany) and the a fixed version of this code can be found in the repository archived at zenodo with [DOI:10.5281/zenodo.7188637](https://doi.org/10.5281/zenodo.7188637). <br>
Further data are not disclosed openly by Gridradar due to the potential misuse by third parties, e.g. the intent to damage critical infrastructure. Access to the data for scientific purposes is possible by contacting [GridRadar](https://gridradar.net) directly.

To obtain the power flow data, we suggest to use the sftp interface to download the dataset on Cross-Border Physical flows.



## Usage
Since the raw frequency data might be to large to be analyzed on a laptop or a local machine, we suggest to split the preprocessing and the subsequent plotting.

### Preprocessing and Analysis

The raw csv files will be pickled and also split for the single measurement stations to reduce ram usage. <br>
Note, if you are using only the frequency data that was uploaded to [DOI:10.5281/zenodo.7188637](https://doi.org/10.5281/zenodo.7188637), you don't need to run the preprocessing step. The data has already been put into a single pickled file for the provided loaction (Bremen, Germany).

```python
>>> from ukraine_power_connection import preprocess_data
>>> preprocess_data.frequency_to_pickle(also_split_files=True)
```
Now, perform the PCA analysis and save the relevant results via

```python
>>> from ukraine_power_connection import pca_analysis
>>> pca_analysis.meta_all_pca()
```

### Plotting 

The plots seen in the publication can created and also be saved in the `ukraine_power_connection/plots/` folder using:

```python
>>> from ukraine_power_connection import paper_plots
>>> paper_plots.plot_everything()
```

Keep in mind that some labels require a latex installation to render correctly.